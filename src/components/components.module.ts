import { NgModule } from '@angular/core';
import { HotelratingComponent } from './hotelrating/hotelrating';
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
	declarations: [HotelratingComponent],
	imports: [BrowserModule],
	exports: [HotelratingComponent]
})
export class ComponentsModule {}
