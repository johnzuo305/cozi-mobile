import { Component ,Input} from '@angular/core';

/**
 * Generated class for the HotelratingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'hotelrating',
  templateUrl: 'hotelrating.html'
})
export class HotelratingComponent {

	 @Input() public hotelRating: number;

  
	createStarRange(starRating){
      var items: number[] = [];
      if (!starRating) {
        return items;
      }
      for(var i = 0; i < Math.floor(starRating); i++){
         items.push(i);
      }
      return items;
    }

    hasHalfStar(starRating) {
    	if (!starRating) {
            return false;
        }
        return (Math.floor(starRating) < starRating);
    }
};
