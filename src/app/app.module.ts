import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {HttpClientModule} from '@angular/common/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpserviceProvider } from '../providers/httpservice/httpservice';
import { LoginPage } from '../pages/login/login';
import { LoginserviceProvider } from '../providers/loginservice/loginservice';
import { CitylistPage } from '../pages/citylist/citylist';
import { SearchhotelsPage } from '../pages/searchhotels/searchhotels';
import { HoteldetailPage } from '../pages/hoteldetail/hoteldetail';
import { OrderlistPage } from '../pages/orderlist/orderlist';
import { OrderdetailPage } from '../pages/orderdetail/orderdetail';
import { AccountinfoPage } from '../pages/accountinfo/accountinfo';
import { HotellistPage } from '../pages/hotellist/hotellist';
import { CityserviceProvider } from '../providers/cityservice/cityservice';
import { CalendarModule } from "ion2-calendar";
import { ToolserviceProvider } from '../providers/toolservice/toolservice';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { LanguageserviceProvider } from '../providers/languageservice/languageservice';
import { ComponentsModule } from '../components/components.module';


// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    CitylistPage,
    SearchhotelsPage,
    HoteldetailPage,
    OrderlistPage,
    OrderdetailPage,
    AccountinfoPage,
    HotellistPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    ComponentsModule,
    TranslateModule.forRoot({
      loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
    }
    ),
    CalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    CitylistPage,
    SearchhotelsPage,
    HoteldetailPage,
    OrderlistPage,
    OrderdetailPage,
    AccountinfoPage,
    HotellistPage  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpserviceProvider,
    LoginserviceProvider,
    CityserviceProvider,
    ToolserviceProvider,
    LanguageserviceProvider
  ]
})
export class AppModule {}
