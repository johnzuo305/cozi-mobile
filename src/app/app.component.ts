import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginPage} from '../pages/login/login';
import { OrderlistPage } from '../pages/orderlist/orderlist';
import { AccountinfoPage } from '../pages/accountinfo/accountinfo';
import { LanguageserviceProvider } from '../providers/languageservice/languageservice';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(public languageserviceProvider: LanguageserviceProvider,
    public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'My orders', component:OrderlistPage},
      { title: 'My account', component:AccountinfoPage}
    ];


      let lanuageCode = null;//saveService.readLanguageSetting();


      if( lanuageCode )
      {
        languageserviceProvider.setDefaultLanguage(lanuageCode);
      }
      else
      {
           // use navigator lang if available
           let userLang = navigator.language.split('-')[0];
           userLang = 'zh_CN';///(zh)/gi.test(userLang) ? 'zh_CN' : 'en_AU';
           languageserviceProvider.setDefaultLanguage(userLang);
      }    

  }



  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
