import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../base/base';
import { HttpserviceProvider } from '../../providers/httpservice/httpservice';
import { LoginserviceProvider } from '../../providers/loginservice/loginservice';

/**
 * Generated class for the AccountinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accountinfo',
  templateUrl: 'accountinfo.html',
})
export class AccountinfoPage extends BasePage{


  accountInfo ;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public loginserviceProvider : LoginserviceProvider, public httpserviceProvider : HttpserviceProvider) {
  	super(navCtrl,navParams);
  }

  ionViewDidLoad() {
  	super.ionViewDidLoad();
    console.log('ionViewDidLoad AccountinfoPage');

    let url = 'partner/getUserAccountDetail/COZITRIP/Tamilselvi';
    this.httpserviceProvider.getData(url).subscribe(result => {console.log(result);

      this.accountInfo = result.payload;
      this.loginserviceProvider.setLoginInfo(result.payload.uuid,result.payload.token,result.agents);
  	}
      );    
  }

    getRoleName(role)
  {
    var nowRoleName = '';
              switch (role) {
            case "COZI_BDM":
            nowRoleName = "BDM User";
              break;
            case "ROLE_PARENT":
            nowRoleName = "Parent User";
              break;
            case "ROLE_USER":
            nowRoleName = "Normal User";
              break;
            case "ROLE_DEMO":
            nowRoleName = "Demo User";
              break;
            case "COZI_ADMIN":
            nowRoleName = "Admin User";
            default:
              break;
          }
     return nowRoleName;
  }

  checkBookPriviege(key)
  {
    if(this.accountInfo.privileges && this.accountInfo.privileges.privilege && this.accountInfo.privileges.privilege.length>0)
    {
      for(var i=0;i<this.accountInfo.privileges.privilege.length;i++)
      {
        if(key==this.accountInfo.privileges.privilege[i])
        {
          return 'YES';
        }
      }
    }

    return 'NO';
  }  


}
