import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-base'
})
export class BasePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log('constructor BasePage');  	
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BasePage');
  }

}
