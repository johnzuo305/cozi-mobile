import { Component ,Output,EventEmitter} from '@angular/core';
import { IonicPage, NavController, NavParams ,ViewController} from 'ionic-angular';
import { BasePage } from '../base/base';
import { CityserviceProvider } from '../../providers/cityservice/cityservice';
import { CityObject } from '../../providers/cityservice/city';
import { LanguageserviceProvider } from '../../providers/languageservice/languageservice';
/**
 * Generated class for the CitylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-citylist',
  templateUrl: 'citylist.html',
})
export class CitylistPage extends BasePage{

  AustraliaCitys = [];
  NewZealand = [];

  inputString = '';

  searchResults = [];

  currentLoadingPre;

  constructor( public languageserviceProvider : LanguageserviceProvider,
    public navCtrl: NavController, public navParams: NavParams ,public cityserviceProvider : CityserviceProvider, public viewCtrl: ViewController) {
  	super(navCtrl,navParams);

  	this.AustraliaCitys = cityserviceProvider.getAustraliaCities();
  	this.NewZealand = cityserviceProvider.getNewLandCities();
  }

  ionViewDidLoad() {
  	super.ionViewDidLoad();
    console.log('ionViewDidLoad CitylistPage');
  }

  filterCities(e: any) {
    this.inputString = e.target.value;
    console.log(this.inputString);

    this.searchResults = this.cityserviceProvider.searchLocalDataCities(this.inputString);
    if(this.currentLoadingPre)
      this.currentLoadingPre.unsubscribe();

    if(this.inputString.length>0)
    {
      let url = `locationService/autocomplete/${this.inputString}/en_AU`;
      this.currentLoadingPre = this.cityserviceProvider.searchCitiesFromGoogle(url).subscribe( result => {
        console.log(result);
        if(result.payload.locations && result.payload.locations.length>0)
        {
          this.searchResults = [];
        }
        for(var city of result.payload.locations)
        {
          let cityObject = new CityObject();
          cityObject.cityName = city.name;
          cityObject.countryName  = city.countryName;
          cityObject.placeId = city.placeId;
          this.searchResults.push(cityObject);
        }
      }
      )
    }
  }


  selectCity(cityObject:CityObject)
  {
    if(cityObject.placeId && cityObject.placeId.length>0)
    {
      let url = `locationService/placeDetail/${cityObject.placeId}/${this.languageserviceProvider.getCurrentLanguage().code}`;
      this.currentLoadingPre = this.cityserviceProvider.searchDestinationGoogle(url).subscribe( result => {
        console.log(result);
        cityObject.lat = result.payload.location.lat;
        cityObject.long = result.payload.location.lng;
        this.viewCtrl.dismiss(cityObject);
        }
      )
    }
    else
    {
      this.viewCtrl.dismiss(cityObject);
    }
  }

}
