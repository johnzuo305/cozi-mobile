import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchhotelsPage } from './searchhotels';


@NgModule({
  declarations: [
    SearchhotelsPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchhotelsPage),
    
  ],
})
export class SearchhotelsPageModule {}
