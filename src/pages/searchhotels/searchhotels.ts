import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../base/base';
import { HoteldetailPage } from '../hoteldetail/hoteldetail';
import { HotelSearchRequest , RoomGuest ,SearchedHotel} from '../../providers/models/hotel-search.model';
import { ToolserviceProvider } from '../../providers/toolservice/toolservice';
import { HttpserviceProvider } from '../../providers/httpservice/httpservice';
import { LoginserviceProvider } from '../../providers/loginservice/loginservice';

/**
 * Generated class for the SearchhotelsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-searchhotels',
  templateUrl: 'searchhotels.html',
})
export class SearchhotelsPage extends BasePage{

  cityObject ;
  arrival;
  departure;

  showHotels = [];

  nights = 1;
  cityCode = '';

  selectSortType = 0;

  maxPage =  0 ;
  pageArray = [];
  pageIndex = 1;
  obserberArray = [];
  totleResults = [];
  numPrePage = 15;

  loadingNum = 0;

  constructor( public loginserviceProvider : LoginserviceProvider,
    public toolserviceProvider : ToolserviceProvider, public httpserviceProvider : HttpserviceProvider,
    public navCtrl: NavController, public navParams: NavParams) {
  		super(navCtrl,navParams);

      this.cityObject = navParams.get("cityObject");
      this.arrival = navParams.get('arrival');
      this.departure = navParams.get('departure');

      console.log(this.cityObject);
      console.log(this.arrival);
      console.log(this.departure);
  }

  ionViewDidLoad() {
  	super.ionViewDidLoad();
    console.log('ionViewDidLoad SearchhotelsPage');

    this.searchHotels();
  }


  showDetail() {
  	this.navCtrl.push(HoteldetailPage);
  }
  
  generatorRequest() {
    let request = new HotelSearchRequest();
    request.agent='HOTELBEDS';
    request.arrival = this.arrival;
    request.departure = this.departure;
    let roomGuest = new RoomGuest();
    roomGuest.adults = 2;
    roomGuest.children = 0;
    roomGuest.childrenAges = '';
    request.roomGuests = [roomGuest];
    request.minPrice = 0;
    request.maxPrice = 1000;
    request.minStar = 3;
    request.maxStar = 5;
    request.cityCode = this.cityObject.cityCode;
    request.latitude = this.cityObject.lat;
    request.longitude = this.cityObject.long;
    request.currency = 'AUD';
    request.rooms = 1;
    request.showCombinedPicture = false;
    request.locale = 'en_AU';
    request.uuid = this.toolserviceProvider.guid();

    this.nights = this.toolserviceProvider.getNightNum(request.arrival,request.departure); 
    this.cityCode = request.cityCode;
    return request;
  }


  getDis(dis,compareNo)
  {
    let disNumber = parseFloat(dis);
    if(disNumber<=compareNo)
    {
      return true;
    }
    return false;
  }

 checkPriceAndRate(results : SearchedHotel[]) : SearchedHotel[]
 {
   // var checkRate = 0;
   // for(var i=0;i<this.rateSelectArray.length;i++)
   // {
   //   if(this.rateSelectArray[i].checked==true)
   //   {
   //     checkRate++;
   //   }
   // }
   // if(checkRate==2)
   // {
   //   for(var i=0;i<results.length;i++)
   //   {
   //     let hotel = results[i];
   //     var minStart = hotel.hotelRating.starRating-1;
   //     if(minStart<0)
   //       minStart = 0;
   //     if(minStart>=this.rateSelectArray.length)
   //       minStart = this.rateSelectArray.length-1;
   //     minStart = Math.ceil(minStart);
   //     if(hotel.hotelRating && hotel.hotelRating.starRating && hotel.hotelRating.starRating>=1
   //       && hotel.hotelRating.starRating<=5
   //       && this.rateSelectArray[minStart].checked==false
   //       )
   //     {
   //       if(this.selectedHotel && this.selectedHotel.uuid==hotel.hotelUuid)
   //       {

   //       }
   //       else
   //       {
   //         results.splice(i,1);
   //         i--;
   //       }
   //     }

   //   }  

   // }

   // var checkPrice = 0;
   // for(var i=0;i<this.priceSelectArray.length;i++)
   // {
   //   if(this.priceSelectArray[i].checked==true)
   //   {
   //     checkPrice++;
   //   }
   // }

   // if(checkPrice>=2 && checkPrice<=3)
   // {
   //   for(var i=0;i<results.length;i++)
   //   {
   //     let hotel = results[i];
   //     var inRanged = false;
   //     let perNightPrice = hotel.secondaryPrice/this.nights;

   //     for(var j=0;j<this.priceSelectArray.length;j++)
   //     {
   //        if(this.priceSelectArray[j].checked==true )
   //        {
   //          if(perNightPrice>=priceAUDRangeArray[j].min
   //            && perNightPrice <=priceAUDRangeArray[j].max)
   //          {
   //            inRanged=true;
   //            continue;
   //          }
   //        }
   //     }

   //     if(inRanged==false)
   //     {
   //       if(this.selectedHotel && this.selectedHotel.uuid==hotel.hotelUuid)
   //       {

   //       }
   //       else
   //       {
   //         results.splice(i,1);
   //         i--;        
   //       } 
   //     }

   //   }       
   // }


   return results;
 }

   sortResults(sortType : number)
    {
        function comparePrice(a,b) {
        if (a.secondaryPrice < b.secondaryPrice)
          return -1;
        if (a.secondaryPrice > b.secondaryPrice)
          return 1;
        return 0;
        }

        function compareRank(a,b) {
        if (a.rank > b.rank)
          return -1;
        if (a.rank < b.rank)
          return 1;
        return 0;
        }


        function compareStar(a,b) {
        if(!a.hotelRating && !b.hotelRating)
          return 0;
        if(!a.hotelRating && b.hotelRating)
          return 1;
        if(a.hotelRating && !b.hotelRating)
          return -1;
        if (a.hotelRating.starRating < b.hotelRating.starRating)
          return 1;
        if (a.hotelRating.starRating > b.hotelRating.starRating)
          return -1;
        return 0;
        }

      if(sortType == 0)
      {
          this.totleResults.sort(comparePrice);
      }
      else if(sortType==1)
      {

          this.totleResults.sort(compareStar);
      }
      else
      {
          this.totleResults.sort(compareRank);
      }

      this.resetShowResult();
    }

    resetShowResult()
    {
        this.pageIndex = 1;
        this.showHotels = this.totleResults.slice(0,this.numPrePage);
    }




  searchHotels( )
  {
     let url = 'b2bWeb/search/0/500';
     let request = this.generatorRequest();

     let nameArray = ["ETA","HOTELBEDS","GTA","AMADEUS","DUMMY"];
     if(this.loginserviceProvider.agents && this.loginserviceProvider.agents.length>0)
     {
       nameArray = this.loginserviceProvider.agents;
     }
     this.loadingNum = nameArray.length;

     this.maxPage =  0 ;
     this.pageArray = [];
     this.pageIndex = 1;
     this.showHotels = [];


     for(var i=0;i<this.obserberArray.length;i++)
     {
       var ober = this.obserberArray[i];
       ober.unsubscribe();
     }

     this.obserberArray = [];


     this.totleResults = [];



     for(var at=0 ; at<nameArray.length;at++)
     {
       var agentRequest = request;
       agentRequest.agent = nameArray[at];

       let ober = this.httpserviceProvider.postData(request,url).subscribe(result => {
          console.log(result);
          if(result.status == 0){
              var oneResult =  this.checkPriceAndRate(result.payload.results);
              console.log('get hotel' + oneResult.length);

              for(var j=0;j<oneResult.length;j++)
              {
                  let newHotel = oneResult[j];
                  var found = false;
                  for(var h=0;h<this.totleResults.length;h++)
                  {
                      let hotel = this.totleResults[h];
                      if(hotel.hotelUuid == newHotel.hotelUuid)
                      {
                        if(newHotel.cachedQuoteUuid)
                        {
                          hotel.uuidArray.push(newHotel.cachedQuoteUuid);
                        }
                        if((newHotel.secondaryPrice<hotel.secondaryPrice && newHotel.secondaryPrice>0)
                           || (hotel.secondaryPrice<=0 && newHotel.secondaryPrice>0) )
                        {
                          hotel.price = newHotel.price;
                          hotel.secondaryPrice = newHotel.secondaryPrice;
                          hotel.freeCancellation = newHotel.freeCancellation;
                          hotel.mealsPlan = newHotel.mealsPlan;
                          hotel.bookingAgent = newHotel.bookingAgent;
                        }
                        found = true;
                          break;
                          }
                        }

                        if(found==false)
                        {
                          newHotel.uuidArray = [];
                          if(newHotel.cachedQuoteUuid)
                          {
                            newHotel.uuidArray.push(newHotel.cachedQuoteUuid);
                          }
                          this.totleResults.push(newHotel);
                        }
                      }

                       this.sortResults(this.selectSortType);
                    }
                    else{
                         console.log('get hotel error' );
                    }       
           },
           err => {
              this.checkResults(request,nameArray);
           },
           () => { 
             console.log('end');
              this.checkResults(request,nameArray);
           }


       );


       this.obserberArray.push(ober);

   
     }
  }

  nextPage()
  {
      if(this.pageIndex >= this.maxPage)
      {
          return;
      }
      this.searchByPageIndex(this.pageIndex +1);
  }

  searchByPageIndex(index)
  {


      if(this.pageIndex == index)
         return;


      this.pageIndex = index;
      this.showHotels = this.totleResults.slice(0,this.pageIndex*this.numPrePage);
  }  


  checkResults(request : HotelSearchRequest,nameArray : string[])
  {

      this.loadingNum = this.loadingNum-1;
      this.moveProgress( (nameArray.length - this.loadingNum) * (100 / nameArray.length));
      
      if(this.loadingNum==0)
       {
            if(this.totleResults.length>0)
            {
                this.maxPage =  Math.ceil(this.totleResults.length / this.numPrePage) ;
                this.pageIndex = 1;
                this.showHotels = this.totleResults.slice(0,this.numPrePage);
            }
      }
      else
      {

      }

  }

  progressWidth = 0;
  moveProgress( percent : number) {
  var elem = document.getElementById("myBar");   
  var myProgress = document.getElementById("myProgress");
  var id = setInterval(frame, 50);

  let that = this;


  function frame() {
    if (that.progressWidth >= percent || (that.loadingNum == 0 && that.totleResults.length<=0)) 
    {
      clearInterval(id);
      if(that.progressWidth>=95|| (that.loadingNum == 0 && that.totleResults.length<=0) )
      {
        that.progressWidth = 0;
        elem.style.width = that.progressWidth + '%'; 
        myProgress.style.display = "none";
      }
    } 
    else {
      myProgress.style.display = "block";
      that.progressWidth++; 
      elem.style.width = that.progressWidth + '%'; 
    }
  }
 }  


   doInfinite(infiniteScroll) {

     if(this.loadingNum>0)
     {
       infiniteScroll.complete();
       return;
     }

    setTimeout(() => {
      this.nextPage();
      infiniteScroll.complete();
    }, 500);
  }

}
