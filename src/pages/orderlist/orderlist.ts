import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../base/base';
import { OrderdetailPage } from '../orderdetail/orderdetail';

/**
 * Generated class for the OrderlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orderlist',
  templateUrl: 'orderlist.html',
})
export class OrderlistPage extends BasePage{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	super(navCtrl,navParams);
  }

  ionViewDidLoad() {
  	super.ionViewDidLoad();
    console.log('ionViewDidLoad OrderlistPage');
  }

  showDetail() {
  	this.navCtrl.push(OrderdetailPage);
  }

}
