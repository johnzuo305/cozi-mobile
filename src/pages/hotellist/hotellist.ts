import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { HttpserviceProvider } from './../../providers/httpservice/httpservice';
import { LoginserviceProvider } from './../../providers/loginservice/loginservice';

/**
 * Generated class for the HotellistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

class AutoCompleteHotelRequest {
	queryString: string;
	cityCode: string;
	locale: string;
	latitude : number;
	longitude : number;
}

@IonicPage()
@Component({
  selector: 'page-hotellist',
  templateUrl: 'hotellist.html',
})
export class HotellistPage {

  //Variable to save the API result.
  hotels=[];
  //String inside the textbox;
  inputString: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpserviceProvider : HttpserviceProvider, public viewCtrl: ViewController) {
  	this.inputString = ' ';
  }

  ionViewDidLoad() {
  	 console.log('ionViewDidLoad HotellistPage');
     this.viewCtrl.setBackButtonText(" ");
  }


  /*
  	Filers hotels via API call. e.target.value gets us the text inside the input box
  	This allows us to simply call the API with that and let NGfor display the information.
  */
  filterHotels(e: any){
    let request = new AutoCompleteHotelRequest();
    this.inputString = e.target.value;
    console.log(this.inputString);
    request.queryString = this.inputString.toLowerCase();
    request.cityCode = null;
    request.locale = null;
    request.latitude = null;
    request.longitude = null;

    //API call.
    let url =  'b2bWeb/preferredHotel'
    this.httpserviceProvider.postData(request,url).subscribe(result => {
      console.log(result);
    	this.hotels = result.payload.hotels;
      });
  }
  /*
	When user clicks on the results of search, assign the inputbox string to the name.
  */
  extractName(name: string){
  	this.inputString = name;
  	console.log(this.inputString);
    this.submitText();
  }

  submitText(){
    //Send data back to the previous screen.
    this.viewCtrl.dismiss(this.inputString);
  }

}
