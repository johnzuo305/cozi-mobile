import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HotellistPage } from './hotellist';

@NgModule({
  declarations: [
    HotellistPage,
  ],
  imports: [
    IonicPageModule.forChild(HotellistPage),
  ],
})
export class HotellistPageModule {}
