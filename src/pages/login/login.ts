import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../base/base';
import { HttpserviceProvider } from './../../providers/httpservice/httpservice';
import { HomePage } from './../../pages/home/home';
import { LoginserviceProvider } from './../../providers/loginservice/loginservice';

class LoginRequest{
  companyId : string;
  accountId : string;
  password : string;
  locale : string;
}


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage extends BasePage{

  constructor(public loginserviceProvider : LoginserviceProvider,
    public navCtrl: NavController, public navParams: NavParams,public httpserviceProvider : HttpserviceProvider) {
  	super(navCtrl,navParams);
  }

  ionViewDidLoad() {
  	super.ionViewDidLoad();
    console.log('ionViewDidLoad LoginPage');

    let request = new LoginRequest();
    request.accountId = 'john';
    request.companyId = 'JOHN';
    request.locale = 'en_AU';
    request.password = '123456';

    let url = 'partner/login';
    this.httpserviceProvider.postData(request,url).subscribe(result => {console.log(result);
      this.loginserviceProvider.setLoginInfo(result.payload.uuid,result.payload.token,result.payload.agents);
      this.navCtrl.setRoot(HomePage);}
      );

  }

}
