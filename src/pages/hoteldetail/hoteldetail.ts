import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BasePage } from '../base/base';
/**
 * Generated class for the HoteldetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-hoteldetail',
  templateUrl: 'hoteldetail.html',
})
export class HoteldetailPage extends BasePage{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	super(navCtrl,navParams);
  }

  ionViewDidLoad() {
  	super.ionViewDidLoad();
    console.log('ionViewDidLoad HoteldetailPage');
  }

  

}
