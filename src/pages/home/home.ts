import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController} from 'ionic-angular';
import { BasePage } from '../base/base';
import { CitylistPage } from '../citylist/citylist';
import { SearchhotelsPage } from '../searchhotels/searchhotels';
import { HotellistPage } from '../hotellist/hotellist';
import { CityObject } from '../../providers/cityservice/city';
import { CalendarModal, CalendarModalOptions, DayConfig } from "ion2-calendar";
import { ToolserviceProvider } from '../../providers/toolservice/toolservice';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends BasePage {

  hotel: String;
  cityObject : CityObject;
  cityName = '悉尼';

  arrival = '';
  departure = '';

  constructor(    public modalCtrl: ModalController, public toolserviceProvider : ToolserviceProvider,
    public navCtrl: NavController, public navParams: NavParams,public modalCntrl:ModalController) {
  	super(navCtrl,navParams);
    console.log('constructor HomePage');
  	this.hotel = '所选酒店';
  }

  ionViewDidLoad() {
  	super.ionViewDidLoad();
    console.log('ionViewDidLoad HomePage');
  }


  searchCity() {

    let cityListPage = this.modalCntrl.create(CitylistPage);
    //Show page.
    cityListPage.present();
    //When page closes, change <p> data with whatever it gets back.
    cityListPage.onDidDismiss(data => {
      if(data != ' '){
        this.cityObject = data;
        this.cityName = this.cityObject.cityName;
      }
    });    
  }

  searchHotel() {
    this.navCtrl.push(SearchhotelsPage,{
      cityObject : this.cityObject,
      arrival : this.arrival,
      departure : this.departure
    });
  }
  //Navigates to hotelList page.
  //Gets called on onlick onthe search box.
  showHotelList(){
    //this.navCtrl.push(HotellistPage);
    //Creates a modal. Temp page for data entry.
    let search = this.modalCntrl.create(HotellistPage);
    //Show page.
    search.present();
    //When page closes, change <p> data with whatever it gets back.
    search.onDidDismiss(data => {
      if(data != ' '){
        this.hotel = data;
      }
    });

  }

     openCalendar() {
        const options: CalendarModalOptions = {
          pickMode: 'range',
          title: '选择日期'
        };
    
        let myCalendar = this.modalCtrl.create(CalendarModal, {
          options: options
        });
    
        myCalendar.present();
    
        myCalendar.onDidDismiss(date => {
          console.log(date);
          if(date && date.from && date.to)
          {
            this.arrival = date.from.string;
            this.departure = date.to.string;
          }
        });

      }    

  removeText(){
    this.hotel = '所选酒店';
  }

  checkInput(){
    if(this.hotel === '所选酒店'){
      return false;
    } else {
      return true;
    }
  }


  
}
