export class CityObject {
  cityName:string;
  cnCityName : string;
  cityCode:string;
  lat:number;
  long:number;
  countryName : string;
  placeId : string;
}