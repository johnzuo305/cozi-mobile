import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CityObject } from './city';
import { Destination } from '../../providers/models/hotel-search.model';
import { AustraliaCityArray,NewZealandCityArray } from './mock-cities';
import { HttpserviceProvider } from '../httpservice/httpservice'

/*
  Generated class for the LoginserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CityserviceProvider {


  constructor(public httpserviceProvider : HttpserviceProvider) {
  }  

  getAustraliaCities(): CityObject[] {
    return AustraliaCityArray;
  }

  getNewLandCities() : CityObject[] {
    return NewZealandCityArray;
  }


  searchDestinationGoogle(url)
  {
    return this.httpserviceProvider.getData(url);
  }


  searchCitiesFromGoogle(url) {
    return this.httpserviceProvider.getData(url);
  }

  searchLocalDataCities(str : string) : CityObject[] {
    let citys = [];
    var english = /^[a-zA-Z0-9_ ]*$/;
    str = str.toLowerCase();
    if(english.test(str) )
    {    
      for(var i=0;i<AustraliaCityArray.length;i++)
      {
        if(AustraliaCityArray[i].cityName.toLowerCase().indexOf(str)>=0)
        {
          citys.push(AustraliaCityArray[i]);
        }
      }
      for(var i=0;i<NewZealandCityArray.length;i++)
      {
        if(NewZealandCityArray[i].cnCityName.indexOf(str)>=0)
        {
          citys.push(NewZealandCityArray[i]);
        }
      }
    }
    else
    {
      for(var i=0;i<AustraliaCityArray.length;i++)
      {
        if(AustraliaCityArray[i].cityName.toLowerCase().indexOf(str)>=0)
        {
          citys.push(AustraliaCityArray[i]);
        }
      }
      for(var i=0;i<NewZealandCityArray.length;i++)
      {
        if(NewZealandCityArray[i].cnCityName.indexOf(str)>=0)
        {
          citys.push(NewZealandCityArray[i]);
        }
      }
    }
    return citys;
  }

  getCityObjectByStr(str : string ) : CityObject {
    var english = /^[a-zA-Z0-9_ /]*$/;
    str = str.toLowerCase();
    var cityObject : CityObject = null;
    if(english.test(str) )
    {    
      for(var i=0;i<AustraliaCityArray.length;i++)
      {
        if(AustraliaCityArray[i].cityName.toLowerCase().indexOf(str)>=0)
        {
          cityObject =  AustraliaCityArray[i];
        }
      }

      for(var i=0;i<NewZealandCityArray.length;i++)
      {
        if(NewZealandCityArray[i].cityName.toLowerCase().indexOf(str)>=0)
        {
          cityObject =  NewZealandCityArray[i];
        }
      }
    }
    else
    {
      for(var i=0;i<AustraliaCityArray.length;i++)
      {
        if(AustraliaCityArray[i].cnCityName.indexOf(str)>=0)
        {
          cityObject =  AustraliaCityArray[i];
        }
      }

      for(var i=0;i<NewZealandCityArray.length;i++)
      {
        if(NewZealandCityArray[i].cnCityName.indexOf(str)>=0)
        {
          cityObject =  NewZealandCityArray[i];
        }
      }
    }
    if(cityObject)
    {
      return cityObject;
    }

    return null;
  }


  getCityObject(cityCode : string ) : CityObject {
    for(var cityObject of AustraliaCityArray)
    {
      if( cityObject.cityCode == cityCode)
        return cityObject;
    }

    for(var cityObject of NewZealandCityArray)
    {
      if( cityObject.cityCode == cityCode)
        return cityObject;
    }


    return null;
  }

  calculateNearestCity(lat,long,cityObject: CityObject) 
  {
    return (cityObject.lat-lat)*(cityObject.lat-lat) +(cityObject.long-long)*(cityObject.long-long);
  }

  getNearestCity(cityCode : string,countryCode : string,latitude : number , longitude : number) : CityObject {
    if(countryCode=='AU')
    {
      var minDis = 10000000;
      var index = 0;
      for(var i=0;i< AustraliaCityArray.length;i++)
      {
        let dis = this.calculateNearestCity(latitude,longitude,AustraliaCityArray[i]);
        if(dis<minDis)
        {
          minDis = dis;
          index = i;
        }
      }
      if(index<AustraliaCityArray.length)
        return AustraliaCityArray[index];
      return AustraliaCityArray[0];
    }
    else if(countryCode=='NZ')
    {
      var minDis = 10000000;
      var index = 0;
      for(var i=0;i< NewZealandCityArray.length;i++)
      {
        let dis = this.calculateNearestCity(latitude,longitude,NewZealandCityArray[i]);
        if(dis<minDis)
        {
          minDis = dis;
          index = i;
        }
      }      
      if(index<NewZealandCityArray.length)
        return NewZealandCityArray[index];
      return NewZealandCityArray[0];    
    }

    return null;
  }

  getDestination(cityCode : string ) : Destination {
    for(var cityObject of AustraliaCityArray)
    {
      if( cityObject.cityCode == cityCode)
      {
        let destination = new Destination()
        destination.cityCode = cityCode;
        destination.countryCode = 'AU';
        destination.name = cityObject.cityName;
        destination.latitude = cityObject.lat;
        destination.longitude = cityObject.long;
        return destination;
      }
    }

    for(var cityObject of NewZealandCityArray)
    {
      if( cityObject.cityCode == cityCode)
      {
        let destination = new Destination()
        destination.cityCode = cityCode;
        destination.countryCode = 'NZ';
        destination.name = cityObject.cityName;
        destination.latitude = cityObject.lat;
        destination.longitude = cityObject.long;
        return destination;
      }
    }
    return null;
  }


}
