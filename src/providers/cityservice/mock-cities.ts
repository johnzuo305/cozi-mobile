import { CityObject } from './city';


 export const AustraliaCityArray : CityObject[]=  [
    {cityName:'Sydney',cnCityName:"悉尼" ,cityCode: "SYD",lat:-33.8688197 ,long:151.2092955 , countryName:"",placeId:""},
    {cityName:'Melbourne',cnCityName:"墨尔本" , cityCode: "MEL",lat:-37.813611 ,long:144.963056, countryName:"",placeId:""},
    {cityName:'Brisbane',cnCityName:"布里斯班" , cityCode: "BNE",lat:-27.4697707 ,long:153.0251235, countryName:"",placeId:""},
    {cityName:'Gold Coast',cnCityName:"黄金海岸" , cityCode: "OOL",lat:-28.016667 ,long:153.39999999999998, countryName:"",placeId:""},
    {cityName:'Cairns',cnCityName:"凯恩斯" , cityCode: "CNS",lat:-16.9185514 ,long:145.7780548, countryName:"",placeId:""},
    {cityName:'Perth',cnCityName:"珀斯" , cityCode: "PER",lat:-31.9505269 ,long:115.86045719999993, countryName:"",placeId:""},
    {cityName:'Adelaide',cnCityName:"阿德莱德" , cityCode: "ADL",lat:-34.9284989 ,long:138.6, countryName:"",placeId:""},
    {cityName:'Canberra',cnCityName:"堪培拉" , cityCode: "CBR",lat:-35.2809368 ,long:149.13, countryName:"",placeId:""},
    {cityName:'Hobart',cnCityName:"霍巴特" , cityCode: "HBA",lat:-42.8821377 ,long:147.3271949, countryName:"",placeId:""},
    {cityName:'Darwin',cnCityName:"达尔文" , cityCode:"DRW",lat:-12.4634403 ,long:130.8456418, countryName:"",placeId:""},
    {cityName:'Launceston',cnCityName:"朗塞斯顿" , cityCode:"LST",lat:-41.43322149999999 ,long:147.1440875, countryName:"",placeId:""},
    {cityName:'Alice Springs',cnCityName:"艾丽丝泉" , cityCode:"ASP",lat:-23.698,long:133.8807471, countryName:"",placeId:""},
    {cityName:"Hamilton Island",cnCityName:"汉密尔顿岛" , cityCode:"HTI",lat:-20.3512228,long:148.95788979999998, countryName:"",placeId:""},
    {cityName:"Kalbarri",cnCityName:"卡尔巴里" , cityCode:"KAX",lat:-27.720376,long:114.16251399999999, countryName:"",placeId:""},
    {cityName:"Kangaroo Island",cnCityName:"袋鼠岛" , cityCode:"KGC",lat:-35.77690000,long:137.77490000, countryName:"",placeId:""},
    {cityName:"Apollo Bay",cnCityName:"阿波罗湾" , cityCode:"XCO",lat:-38.7569444444,long:143.6694444444, countryName:"",placeId:""},
    {cityName:"Tangalooma",cnCityName:"天阁露玛" , cityCode:"TAN",lat:-27.181151,long:153.371728, countryName:"",placeId:""},
    {cityName:"Warrnambool",cnCityName:"瓦南布尔" , cityCode:"WMB",lat:-38.368678,long:142.498209, countryName:"",placeId:""},
    {cityName:"Phillip Island",cnCityName:"菲利普岛" , cityCode:"PHI",lat:-38.489870,long:145.203820, countryName:"",placeId:""},
    {cityName:"Geelong",cnCityName:"吉朗" , cityCode:"GEX",lat:-38.149918,long:144.361719, countryName:"",placeId:""},
    {cityName:"Werribee",cnCityName:"韦里比" , cityCode:"AVV",lat:-37.9,long:144.664, countryName:"",placeId:""},
    {cityName:"Port Campbell",cnCityName:"坎贝尔港" , cityCode:"VIC",lat:-38.592047,long:142.99956, countryName:"",placeId:""},
    {cityName:"Whitsunday Islands / Proserpine",cnCityName:"圣灵群岛 / 普罗瑟派恩" , cityCode:"PPP",lat:-20.404979,long:148.580765, countryName:"",placeId:""},
    {cityName:"Kings Canyon",cnCityName:"国王峡谷" , cityCode:"KBJ",lat:-24.250641,long:131.511454, countryName:"",placeId:""},
    {cityName:"Yulara",cnCityName:"艾雅斯岩" , cityCode:"AYQ",lat:-25.233494,long:130.98487, countryName:"",placeId:""}
  ];


  export const NewZealandCityArray : CityObject[] = [
      {cityName:'Auckland', cnCityName:"奥克兰",cityCode: "AKL",lat:-36.8484597 ,long:174.7633315, countryName:"",placeId:""},
      {cityName:'Queenstown',cnCityName:"皇后镇", cityCode: "ZQN",lat:-45.0311622 ,long:168.6626435, countryName:"",placeId:""},
      {cityName:'Christchurch',cnCityName:"基督城", cityCode: "CHC",lat:-43.5320544 ,long:172.6362254, countryName:"",placeId:""},
      {cityName:'Lake Tekapo',cnCityName:"特卡波湖", cityCode: "ZTL",lat:-44.0046736 ,long:170.4771212, countryName:"",placeId:""},
      {cityName:'Rotorua', cnCityName:"罗托鲁瓦",cityCode: "ROT",lat:-38.1368478 ,long:176.2497461, countryName:"",placeId:""},
      {cityName:'Dunedin', cnCityName:"但尼丁",cityCode: "DUD",lat:-45.87876 ,long:170.5027976, countryName:"",placeId:""},
      {cityName:'Wellington', cnCityName:"惠灵顿",cityCode: "WLG",lat:-41.2864603 ,long:174.776236, countryName:"",placeId:""},
      {cityName:'Oamaru',cnCityName:"奥玛鲁", cityCode:"OAM",lat:-45.097512,long:170.970415, countryName:"",placeId:""},
      {cityName:'Greymouth', cnCityName:"格雷茅斯",cityCode: "GMN",lat:-42.450392 ,long:171.210762, countryName:"",placeId:""},
      {cityName:'Fox Glacier',cnCityName:"福克斯冰川", cityCode:"FGL",lat:-43.46448,long:170.017588, countryName:"",placeId:""},
      {cityName:'Kaikoura', cnCityName:"凯库拉",cityCode: "KBZ",lat:-42.400817 ,long:173.681386, countryName:"",placeId:""},
      {cityName:'Wanaka',cnCityName:"瓦纳卡湖", cityCode:"WKA",lat:-44.703181,long:169.132098, countryName:"",placeId:""},
      {cityName:'Mount Cook',cnCityName:"库克山", cityCode:"MON",lat:-43.594975,long:170.141788, countryName:"",placeId:""},
      {cityName:"Te Anau",cnCityName:"蒂阿瑙" , cityCode:"TEU",lat:-45.414451,long:167.718053, countryName:"",placeId:""}
    ];