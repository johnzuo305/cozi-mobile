import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Language } from './language.model';
import {TranslateService} from '@ngx-translate/core';

/*
  Generated class for the LanguageserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LanguageserviceProvider {
    private language: Language;
	constructor(private translateService : TranslateService) {
//		this.language = this.getDefaultLanguage();
	}

	getCurrentLanguage() {
		return this.language;
	}

	// getDefaultLanguage() {
	// 	return LANGUAGES[0];
	// }

	setDefaultLanguage(code : string)
	{
		for(var i =0;i<LANGUAGES.length ;i++)
		{
			let language = LANGUAGES[i];
			if(language.code == code)
			{
				this.language = language;
				this.translateService.use(this.language.code);
//				this.saveService.saveLanguageSetting(this.language.code);
				return;
			}
		}

		this.language =  LANGUAGES[1];
		this.translateService.use(this.language.code);
//		this.saveService.saveLanguageSetting(this.language.code);
	}



	getLanguageArray() : Language[]
	{
		return LANGUAGES;
	}

}

export var LANGUAGES: Language[] = [
	{
		index: 0,
		name: "中文",
		code: "zh_CN",
		flag:"flag-icon-cn"
	},
	{
		index: 1,
		name: "English",
		code: "en_AU",
		flag:"flag-icon-au"

	}
];

