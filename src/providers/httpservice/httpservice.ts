import { HttpClient ,HttpHeaders,HttpParams,HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/retry'; 
import 'rxjs/add/operator/finally';

import { LoginserviceProvider } from './../loginservice/loginservice';

/*
  Generated class for the HttpserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HttpserviceProvider {

  baseUrl: string= 'https://cicadaweb-uat.cozitrip.com';
  constructor(public http: HttpClient , public loginserviceProvider : LoginserviceProvider) {
    console.log('Hello HttpserviceProvider Provider');
  }

  postData(request,apiUrl) : Observable<any>
  {
      let body = JSON.stringify(request);
      console.log(body);
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      headers.append('Accept','application/json');

      // let httpParams = new HttpParams();
      // if(this.loginserviceProvider.uuid && this.loginserviceProvider.uuid.length>0
      // && this.loginserviceProvider.token && this.loginserviceProvider.token.length>0)
      // {
      //   httpParams.append('cid', this.loginserviceProvider.uuid);
      //   httpParams.append('token',this.loginserviceProvider.token);
      // }

      let url = `${this.baseUrl}/${apiUrl}?cid=${this.loginserviceProvider.uuid}&token=${this.loginserviceProvider.token}`;
      console.log(url);

      return this.http.post(url,body,{headers: headers}).finally(() => true).catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }

        // ...optionally return a default fallback value so app can continue (pick one)
        // which could be a default value
        // return Observable.of({my: "default value..."});
        // or simply an empty observable
        return Observable.empty();
      });
  }

  getData(apiUrl) : Observable<any>
  {
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      headers.append('Accept','application/json');

      // let httpParams = new HttpParams();
      // if(this.loginserviceProvider.uuid && this.loginserviceProvider.uuid.length>0
      // && this.loginserviceProvider.token && this.loginserviceProvider.token.length>0)
      // {
      //   httpParams.set('cid', this.loginserviceProvider.uuid);
      //   httpParams.set('token',this.loginserviceProvider.token);
      // }

      let url = `${this.baseUrl}/${apiUrl}?cid=${this.loginserviceProvider.uuid}&token=${this.loginserviceProvider.token}`;
      console.log(url);

      return this.http.get(url,{headers: headers}).catch((err: HttpErrorResponse) => {

        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          console.error('An error occurred:', err.error.message);
        } else {
          // The backend returned an unsuccessful response code.
          // The response body may contain clues as to what went wrong,
          console.error(`Backend returned code ${err.status}, body was: ${err.error}`);
        }

        // ...optionally return a default fallback value so app can continue (pick one)
        // which could be a default value
        // return Observable.of({my: "default value..."});
        // or simply an empty observable
        return Observable.empty();
      });
  }  

}
