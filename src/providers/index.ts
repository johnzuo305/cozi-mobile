import { HttpserviceProvider } from './httpservice/httpservice';
import { LoginserviceProvider } from './loginservice/loginservice';
import { CityserviceProvider } from './cityservice/cityservice';
import { ErrorInterceptor } from './errorinterceptor';
