import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LoginserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginserviceProvider {

  uuid='';
  token='';
  agents = [];
  constructor(public http: HttpClient) {
    console.log('Hello LoginserviceProvider Provider');
  }

  setLoginInfo(uuid,token,agents)
  {
  	this.uuid = uuid;
  	this.token = token;
    this.agents = agents;
  }

}
