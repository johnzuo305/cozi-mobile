


export class Destination {
    countryCode: string;
    cityCode:string;
    name:string;
 	latitude : number;
    longitude : number;
    placeId : string;
};



export class RoomGuest
{
	adults: number;
	children:number;
	childrenAges:string;
}



export class HotelSearchRequest {
	showCombinedPicture : boolean;
	uuid: string;
	locale : string;
	cityCode: string;
	arrival: string;
	departure: string;
	preferenceFreetext:string;
	rooms: number;
	currency : string;
    maxPrice : number;
    minPrice : number;
	roomGuests : RoomGuest[];
	minStar:number;
	maxStar:number;
	freeBreakfast:boolean;
	sortType: string;
	preferredHotels:string[];
	bedType:string;
	agent : string;
	latitude : number;
    longitude : number;
    placeId : string;
    // If the cached quotes are expired, we can use this hotel uuid to restore the cached when getting room types.
	hotelUuid: string;
};


export class SearchedHotel {
	cachedQuoteUuid: string;
	uuidArray : string[];
	requestUuid: string;
    hotelUuid: string;
    bookingAgent:string;
	hotelName: string;
	thumbNailUrl: string;
	originalName:string;
	insideIndex:number;
	price: number;
	mealsPlan:{
		breakfast:boolean;
		description:string;
	};
	rank:number;
	currency:string;
	freeCancellation:boolean;
 	secondaryPrice: number;
    secondaryCurrency: string;
	mapDetails: {
		latitude: number;
		longitude: number;
	};
	hotelRating: {
               type:string;
               starRating: number;
            };
	distanceToCbd : string;
	distanceToAirport: string;

};



