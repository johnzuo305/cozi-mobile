import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ToolserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ToolserviceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ToolserviceProvider Provider');
  }

  guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
  }

  priceInteger(price){    
      return Math.ceil(price);
  }

  priceIntegerFloor(price){
    return Math.floor(price);
  }

  priceIntergerRound(price)
  {
    return Math.round(price);
  }

  getNightNum(arrivalDateString : string ,departureDateString  : string ) : number
  {
    let aDate = this.dateFormatBackToDate(arrivalDateString);
    let dDate = this.dateFormatBackToDate(departureDateString);

    let timeDiff = Math.abs(dDate.getTime() - aDate.getTime());
    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
    return diffDays;
  }

  dateFormatBackToDate(dateString) 
  {
    if(dateString)
    {
      var array = dateString.split('-');
      if(array.length==3)
      {
          var d = new Date(array[0],array[1]-1,array[2]);
          return d;
      }

    }

    return null;

  }


   getCorrectDateString(date : Date) : string
     {
      var day = date.getDate();
      var monthIndex = date.getMonth()+1;
      var year = date.getFullYear();

      var str = year + "-";

      if(monthIndex<10)
      {
          str = str + "0" + monthIndex;
      }
      else
      {
          str = str + monthIndex;
      }

      if (day<10)
      {
          str = str + "-" + "0" + day;
      }
      else
      {
          str = str + "-" + day;
      }

      return str;

     }


}
